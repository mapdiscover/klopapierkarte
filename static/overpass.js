var icon = L.icon({
	iconUrl: "toilet-paper.png",
	iconSize: [32,32],
	iconAnchor: [0,0],
	popupAnchor: [14,0]
});
function locationFound(e) {
	map.setView(e.latlng, 14);
	showGlobalPopup("Dein Standort.");
}
function locationError(e) {
	showGlobalPopup("Standort nicht ermittelbar.");
}
function locateNewArea(e) {
	loadPOIS();
}
function buildOverpassApiUrl(map, bounds) {
	if (bounds == "") {
		bounds = map.getBounds().getSouth() + ',' + map.getBounds().getWest() + ',' + map.getBounds().getNorth() + ',' + map.getBounds().getEast();
	}
	query = 'nwr["shop"="supermarket"]' + '(' + bounds + ');nwr["shop"="chemist"]' + '(' + bounds + ');'
	var query = '?data=[out:json][timeout:25];(' + query +');out body center;';
	var baseUrl = 'https://overpass-api.de/api/interpreter';
	var resultUrl = baseUrl + query;
	return resultUrl;
}
function loadPOIS(bounds="") {
	spinner(true);
	Layergroup.clearLayers();
	var overpassApiUrl = buildOverpassApiUrl(map, bounds);
	$.get(overpassApiUrl, function (osmDataAsJson) {
		var resultAsGeojson = osmtogeojson(osmDataAsJson);
		var resultLayer = L.geoJson(resultAsGeojson, {
			style: function (feature) {
				return {color: "#ff0000"};
			},
			filter: function (feature, layer) {
				var isPolygon = (feature.geometry) && (feature.geometry.type !== undefined) && (feature.geometry.type === "Polygon");
				if (isPolygon) {
					feature.geometry.type = "Point";
					var polygonCenter = L.latLngBounds(feature.geometry.coordinates[0]).getCenter();
					feature.geometry.coordinates = [ polygonCenter.lat, polygonCenter.lng ];
				}
			return true;
			},
			onEachFeature: function (feature, layer) {
				// declare variables
				var popupContent = "";
				
				// Add address to POI details view
				popupContent += "<h1>Liebe*r Hamster*in, hier gibt es Klopapier ...</h1><h2>... und theoretisch sogar für euch alle !!! </h2><h3>Beweise, dass du ein Mensch bist und sei solidarisch! Hast du noch mehr als sechs Rollen auf Vorrat, bleib zu Hause.</h3><p>Klopapierkarte by <a href='https://mapdiscover.org/'>MapDiscover Team</a></p><br/><br/><address>Marker Icon made by <a href='https://www.flaticon.com/authors/freepik' title='Freepik'>Freepik</a> from <a href='https://www.flaticon.com/' title='Flaticon'> www.flaticon.com</a></address>";
				layer.options.icon = icon;
				layer.bindPopup(popupContent);
			}
			}).addTo(Layergroup);
	map.addLayer(Layergroup);
	spinner(false);
	});
}
function getStateFromHash() {
	var hash = location.hash;
	if (hash != "") {
		hash = hash.replace("#", "").split("&");
		zoomLevel = Number(hash[0]);
		saved_lat = Number(hash[1]);
		saved_lon = Number(hash[2]);
		map.setView([saved_lat, saved_lon], zoomLevel);
	}
}
function requestLocation() {map.locate({setView: true, zoom: zoomLevel});}
function hashCoords(e) {
location.hash = String(map.getZoom()) + "&" + String(e.latlng.lat).split(".")[0] + "." + String(e.latlng.lat).split(".")[1].slice(0, 4) + "&" + String(e.latlng.lng).split(".")[0] + "." + String(e.latlng.lng).split(".")[1].slice(0, 4);
}
//init map
var map = L.map('map')
map.options.maxZoom = 17;
map.options.minZoom = 10;
map.setView([saved_lat, saved_lon], zoomLevel);
getStateFromHash();
map.on("zoom", function() {zoomLevel = String(map.getZoom());})
map.on("locationfound", locationFound);
map.on("locationerror", locationError);
map.on("click", function(e) {location.hash = String(map.getZoom()) + "&" + String(e.latlng.lat) + "&" + String(e.latlng.lng);})
map.on("moveend", locateNewArea);
var Layergroup = new L.LayerGroup();
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 17,
	attribution: "<a target='_blank' href='https://mapdiscover.org/imprint.html'>Impressum</a> | &copy; <a href='https://openstreetmap.org/copyright'>OpenStreetMap</a> data under <a href='https://opendatacommons.org/licenses/odbl/'>ODbL</a>, Tiles OSMF: <a href='https://creativecommons.org/licenses/by-sa/2.0/'>CC BY-SA</a>"
}).addTo(map);
requestLocation();

loadPOIS();
