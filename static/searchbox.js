var saved_lat = 51.259941;
var saved_lon = 7.166900;
var zoomLevel = 14;
var message;
function showGlobalPopup(m) {
	message = m
	setTimeout(function() {
		document.getElementById("infoPopup").innerHTML = message;
		document.getElementById("infoPopup").style.display = "block";
		setTimeout(function() {
			document.getElementById("infoPopup").style.display = "none";
		}, 3000);
	}, 1000);
}
function spinner(value) { //Show/hide spinner
	var elem = document.getElementById("spinner");
	if (value) { //Show spinner
		elem.style.display = "flex";
	} else { //Hide spinner
		elem.style.display = "none";
	}
}
